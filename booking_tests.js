const assert = require('assert');

describe('booking tests', () => {


    it('shouldnt login with invalid creds', () => {
        browser.url('https://www.booking.com/');
        const loginButton = $$('div.sign_in_wrapper > span')[2];
        loginButton.click();
        //enter invalid email
        const email = $('#username');
        email.setValue('invalidEmail@gmail.com');
        const next = $('span.bui-button__text');
        next.click();
        browser.pause(1000);
        let validation = $('#username-error');
        var isExist = validation.isExisting();
        assert.equal(isExist, true);
        var validationText = validation.getText();
        assert.equal(validationText, "Схоже до цієї адреси не прив\'язано жодного акаунта. Ви можете створити акаунт, щоб почати користуватися нашими послугами.");
        
        //enter valid email and invalid pass
        email.setValue("lubovt404@gmail.com");
        
        next.click();
        browser.pause(1000);

        const password = $('#password');
        password.setValue('12345678');
        const submit = $('button[type="submit"]');
        submit.click();
        browser.pause(1000);
        let passValidation = $('#password-error');
         isExistPassValidation = passValidation.isExisting();
        assert.equal(isExistPassValidation, true);
         passValidationText = passValidation.getText();
        assert.equal(passValidationText, "Електронна адреса та пароль не збігаються.");
    });

    xit('should sign up to newsletter with valid email', () => {
        browser.url('https://www.booking.com/');
        const subscription = $('input.js-searchform-subscribe-box-textfield');
        //invalid email
        subscription.setValue("fdsf@dfds");
        const subscribe = $('button.emk-banner__form-button');
        subscribe.click();
        const arrayValidationMessage = $$('p.-invalid'); 
        const validationMessage = $$('p.-invalid')[0];
        const validationMessageExist = arrayValidationMessage[3].isDisplayed();
        const arraySuccessSubscription = $$('p.-success');
        
        const successSubscriptionExist = arraySuccessSubscription[0].isDisplayed();
        browser.pause(1000);
        assert.equal(validationMessageExist, true);
        //assert.equal(successSubscriptionExist, false);
        //valid email
        subscription.setValue("fdsf@dfds.com");
        subscribe.click();
        
        browser.pause(1000);
        assert.equal(successSubscriptionExist, true);
        assert.equal(validationMessageExist, false);


    });
})