const BookingLoginPage = require('../page/booking_login_po');
const page = new BookingLoginPage();



class BookingLoginActions {

    clickEnterAccount(){
        
        page.loginButton.waitForDisplayed(2000);
        page.loginButton.click();
    }

    enterEmailAndClickNext(value){
        page.emailField.waitForDisplayed(2000);
        page.emailField.setValue(value);
        page.next.click();
    }

    clickLanguageList(){
        page.languageList.waitForDisplayed(2000);
        page.languageList.click();
    }

    selectLanguage(language){
        const selectedLanguage = $(`option[value = "${language}"]`);
        
        selectedLanguage.click();
    }

}

module.exports = BookingLoginActions;