const assert = require('assert');
const BookingLoginActions = require('../Booking_Login/actions/booking_login_pa');
const MainPageLogin = new BookingLoginActions();
const credentials = require('./../../testData.json');

const Assert = require('../../helpers/validators');
const Help = require('../../helpers/helpers');
const help = new Help();
const Wait = require('../../helpers/waiters');


describe('booking login tests', () => {

    beforeEach(() => {
        browser.maximizeWindow();
        browser.url(credentials.bookingUrl);
               
    });

    afterEach(() => {
        browser.reloadSession();
    });


    it('shouldnt login with invalid email', () => {
        
        MainPageLogin.clickEnterAccount();
        //enter invalid email
       MainPageLogin.enterEmailAndClickNext(credentials.invalidEmail);
       help.selectLanguageFromList(credentials.selectedLanguage);
       Wait.forValidationTextChanged();
      
       Assert.errorValidationTextIs(credentials.invalidEmailValidationText);



        //browser.pause(1000);
        // let validation = $('#username-error');
        // var isExist = validation.isExisting();
        // assert.equal(isExist, true);
        // var validationText = validation.getText();
        //assert.equal(validationText, "Схоже до цієї адреси не прив\'язано жодного акаунта. Ви можете створити акаунт, щоб почати користуватися нашими послугами.");
        
        //enter valid email and invalid pass





        // email.setValue("lubovt404@gmail.com");
        
        // next.click();
        // browser.pause(1000);

        // const password = $('#password');
        // password.setValue('12345678');
        // const submit = $('button[type="submit"]');
        // submit.click();
        // browser.pause(1000);
        // let passValidation = $('#password-error');
        //  isExistPassValidation = passValidation.isExisting();
        // assert.equal(isExistPassValidation, true);
        //  passValidationText = passValidation.getText();
        // assert.equal(passValidationText, "Електронна адреса та пароль не збігаються.");
    });

   
})