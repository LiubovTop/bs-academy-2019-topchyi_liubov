

class BookingLoginPage {
    get loginButton () {return $$('div.sign_in_wrapper > span')[2]};
    get emailField (){ return $('#username')};
    get next () {return $('span.bui-button__text')};
    get invalidEmailValidation () {return $('#username-error')};
    get languageList () {return $('#lang-sel-sm')};
    get changeLanguage () {return $('div.app--loading.nw-loader')};
    
    
}

module.exports = BookingLoginPage;