const assert = require('assert');
const {URL} = require('url');
const path = require('path');

describe('tests for hedonist', () => {
   

    xit('shouldnt login with invalid creds', () => {

        browser.url('https://staging.bsa-hedonist.online/');//go to page

        const emailField = $('input[name=email]');//locate email
        const passField = $('input[type=password]');// locate pass
        const enterButton = $('button.button.is-primary');// locate button
        const validation = $('div.toast');

        //submit empty fields

        emailField.setValue('');
        passField.setValue('');
        enterButton.click();
        //expect(validation).to.be.displayed();
        var validationDisplayed = validation.isDisplayed();
        assert.equal(validationDisplayed, true);

        //submit invalid email

        emailField.setValue('lubovt404@gmail.co');
        passField.setValue('lubovt1');
        enterButton.click();
        //expect(validation).to.be.displayed();
        assert.equal(validationDisplayed, true);

        // submit invalid pass
        emailField.setValue('lubovt404@gmail.com');
        passField.setValue('lubovt');
        enterButton.click();
        //expect(validation).to.be.displayed();
        assert.equal(validationDisplayed, true);
    });

    xit('new user registration', () => {
        browser.url('https://staging.bsa-hedonist.online/');//go to page

        const createNew = $('a.link-signup'); // create new link
        const signUp = $('a[href*="/signup"]');// signUp link
        const title = $('h3.title');// title at Create new account page
        const firstName = $('input[name=firstName]');// first name
        const lastName = $('input[name=lastName]'); // last name
        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const createButton = $('button.is-primary');
        const successRegistration = $('div.notices.is-top > div.toast.is-success > div');

        createNew.click();
        assert.equal(title.getText(),"Create new account");// check if get create new account page
        firstName.setValue('Name');
        lastName.setValue('Lastname');
        emailField.setValue('validemail6@gmail.com');
        passField.setValue('123456');
        createButton.click(); // filled in all fields and click 'create new user'
        // assert that success message appeared
        browser.pause(2000);
        assert.equal(successRegistration.getText(),'You have successfully registered! Now you need to login');

    });

    it('should create new place', () => {

        browser.url('https://staging.bsa-hedonist.online/');//go to page

        const emailField = $('input[name=email]');//locate email
        const passField = $('input[type=password]');// locate pass
        const enterButton = $('button.button.is-primary');// locate button
       

        //login

        emailField.setValue('lubovt404@gmail.com');
        passField.setValue('lubovt1');
        enterButton.click();

        browser.pause(3000);
        const menu = $('span.profile__name');
       

        const url = new URL (browser.getUrl());
        const actualUrl = url.hostname.toString() + url.pathname.toString();
        assert.equal(actualUrl,'staging.bsa-hedonist.online/search');

        //const dropdown = $('div.navbar-dropdown');
        const addNewPlace = $('a[href*="/places/add"]');// locate add new place link
        //const menu = $('span.profile__name');
        menu.click();
        addNewPlace.click();

        const name = $('input.is-medium');// locate name of the place
       
        //const city = $$('input.input')[3];
        const city = $('div.place-location input');
        const zip = $$('div.control > input')[4];
        const address = $$('div.control > input')[5];
        const webSite = $$('div.control > input')[6];
        const phone = $('input[type="tel"]');
        const description = $('.textarea');
        const next = $$('div.buttons.is-centered > span.button.is-success')[0];

        let newname = name.setValue("Restaurant Pikachu");
        browser.pause(3000);
        city.setValue("Kiev");
        zip.setValue('03056');
        address.setValue('Vyborgskaya st. 18');
        webSite.setValue('https://pikachu.net');
        phone.setValue(+380442548475);
        description.setValue('The best family restaurant near you!');
        
        next.click();
        //next page Photos
        browser.pause(3000);
        const upload = $('section.section');
        upload.click();

        const filePath = path.join(__dirname, '\Users\Lubov_T\Pictures');

        const remoteFilePath = browser.uploadFile(filePath);
        upload.setValue(remoteFilePath);

        const next1 = $$('div.buttons.is-centered > span.button.is-success')[1];
        next1.click();

        // Locaion (skip)

        const next2 = $$('div.buttons.is-centered > span.button.is-success')[2];
        next2.click();

        //Categories
        const selectCategory = $$('span.select select')[0];
        let category = selectCategory.selectByVisibleText('*\sCafe*\s');
        const addTags = $$('span.select select')[1];
        addTags.selectByVisibleText('Youth cafe');//disabled on page

        const next3 = $$('div.buttons.is-centered > span.button.is-success')[3];
        next3.click();

        //Features

        const featureMusicSetTrue = $$('span.check')[11];
        featureMusicSetTrue.click();
        const next4 = $$('div.buttons.is-centered > span.button.is-success')[4];
        next4.click();

        //Hours

        const monday = $('input[value="mo"]');
        const tuesday = $('input[value="tu"]');
        const wednesday = $('input[value="we"]');
        const thursday = $('input[value="th"]');
        const friday = $('input[value="fr"]');
        const saturday = $('input[value="sa"]');
        const sunday = $('input[value="su"]');

        monday.click();
        tuesday.click();
        wednesday.click();
        thursday.click();
        friday.click();
        saturday.click();
        sunday.click();
        const addHoursButton = $('a.button.is-primary');
        addHoursButton.click();
        const next5 = $$('div.buttons.is-centered > span.button.is-success')[5];
        next5.click();

        //Confirm adding new place
        const confirmTitle = browser.getElementText('h5');
        assert.equal(confirmTitle, "Confirm add place \"" + newname + "?\"");
        const confirmCreation = $$('span.button.is-success')[6];
        confirmCreation.click();

        browser.pause(1000);

        //Checking place page

        assert.equal(browser.getElementText($('div.place-venue__place-name')), newname);
        assert.equal(browser.getElementText($('div.place-venue__category')), category);

        



    });

});