class RegistrationPage {

    get registrationPageTitle () {return $('h3.title')};
    get registrationFirstNameInput () {return  $('input[name=firstName]')};
    get registrationLastNameInput () {return $('input[name=lastName]')};
    get registrationEmailFieldInput () {return $('input[name=email]')};
    get registrationPasswordFieldInput () {return  $('input[type=password]')};
    get buttonCreateNewUser () {return $('button.is-primary')};
    get successRegistrationMessage () {return $('div.notices.is-top > div.toast.is-success > div')};
    

};

module.exports = RegistrationPage;