const assert = require('assert');
const LoginActions = require('../Login/actions/login_pa');
const loginSteps = new LoginActions();
const RegistrationActions = require('./actions/registration_pa');
const registrationSteps = new RegistrationActions();
const credentials = require('./../testData.json');



describe('registration tests on hedonist'), () => {

    beforeEach(() => {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);

       
    });

    afterEach(() => {
        browser.reloadSession();
    });



xit('new user registration though Create new link', () => {
    loginSteps.clickCreateNewLink();    
   assert.equal(registrationSteps.getPageHeader(),credentials.pageHeader);
   registrationSteps.enterFirstName(credentials.newUserName);
   registrationSteps.enterLastName(credentials.newUserLastName);
   registrationSteps.enterEmailForRegistration(credentials.emailForRegistration);
   registrationSteps.enterPasswordForRegistration(credentials.passwordForRegistration);
   registrationSteps.clickCreateNewButton();    
    assert.equal(registrationSteps.getSuccessMessage(),credentials.successMessage);

});
}