const RegistrationPage = require('../page/registration_po.js');
const registration = new RegistrationPage();

class RegistrationActions {

    getPageHeader(){
        registration.registrationPageTitle.waitForDisplayed(2000);
        return registration.registrationPageTitle.getText();
    }

    enterFirstName(firstName){
        registration.registrationFirstNameInput.waitForDisplayed(2000);
        registration.registration.clearValue();
        registration.registration.setValue(firstName);
    }

    enterLastName(lastName){
        registration.registrationLastNameInput.waitForDisplayed(2000);
        registration.registrationLastNameInput.clearValue();
        registration.registrationLastNameInput.setValue(lastName);
    }

    enterEmailForRegistration(registrationEmail){
        registration.registrationEmailFieldInput.waitForDisplayed(2000);
        registration.registrationEmailFieldInput.clearValue();
        registration.registrationEmailFieldInput.setValue(registrationEmail);
    }

    enterPasswordForRegistration(registrationPassword){
        registration.registrationPasswordFieldInput.waitForDisplayed(2000);
        registration.registrationPasswordFieldInput.clearValue();
        registration.registrationPasswordFieldInput.setValue(registrationPassword);
    }

    clickCreateNewButton(){
        registration.buttonCreateNewUser.waitForDisplayed(2000);
        registration.buttonCreateNewUser.click();
    }

    getSuccessMessage(){
        registration.getSuccessMessage.waitForDisplayed(2000);
        return registration.getSuccessMessage.getText();
    }

    


}

module.exports = RegistrationActions;