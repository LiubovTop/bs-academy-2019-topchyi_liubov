const assert = require('assert');
const LoginActions = require('./actions/login_pa.js');

const loginSteps = new LoginActions();
const credentials = require('./../testData.json');



function Login(email, password) {
    loginSteps.enterEmail(email);
    loginSteps.enterPassword(password);
    loginSteps.clickLogin();
}


describe('login tests for hedonist', () => {

    beforeEach(() => {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);
       
    });

    afterEach(() => {
        browser.reloadSession();
    });

   

    xit('shouldnt login with empty fields', () => {

        Login(credentials.emptyEmail,credentials.emptyPassword);
        assert.equal(loginSteps.getValidationDisplayed, true);
    });

    xit('shouldnt login with invalid email', () => {
        Login(credentials.invalidEmail,credentials.validPassword);
        assert.equal(loginSteps.getValidationDisplayed, true);
    });

    xit('shouldnt login with invalid password', () => {

        Login(credentials.validEmail,credentials.invalidPassword);
        assert.equal(loginSteps.getValidationDisplayed, true);

    });

    

});